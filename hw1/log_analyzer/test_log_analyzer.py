from unittest import TestCase
from unittest.mock import Mock, MagicMock, patch

import datetime
import json

from io import StringIO

from log_analyzer import find_latest_logfile, LogInfo
from log_analyzer import construct_settings, config
from log_analyzer import parse_line
from log_analyzer import get_log_data, parse_log_and_collect_timings
from log_analyzer import analyze_timings
from log_analyzer import parse_log
from log_analyzer import main

import sys
import os
import shutil


class TestFindLatestLogfile(TestCase):

    def setUp(self):
        self.path = '/some/path'

    @patch('os.path.exists', Mock(return_value=False))
    def test_no_logs_directory(self):
        self.assertEqual(find_latest_logfile(self.path), None)

    @patch('os.path.exists', Mock(return_value=True))
    @patch('os.listdir', Mock(return_value=['file1', 'file2', 'file3']))
    def test_no_supported_logfile(self):
        self.assertEqual(find_latest_logfile(self.path), None)

    @patch('os.path.exists', Mock(return_value=True))
    @patch('os.listdir',
           Mock(return_value=['file1', 'nginx-access-ui.log-20170630.log', 'nginx-access-ui.log-20181020.gz']))
    def test_only_supported_extensions_1(self):
        self.assertEqual(find_latest_logfile(self.path),
                         LogInfo('nginx-access-ui.log-20181020.gz', datetime.datetime(2018,10,20), 'gz'))

    @patch('os.path.exists', Mock(return_value=True))
    @patch('os.listdir',
           Mock(return_value=['file1', 'nginx-access-ui.log-20170630.gz', 'nginx-access-ui.log-20181120.log']))
    def test_only_supported_extensions_2(self):
        self.assertEqual(find_latest_logfile(self.path),
                         LogInfo('nginx-access-ui.log-20181120.log', datetime.datetime(2018, 11, 20), 'log'))

    @patch('os.path.exists', Mock(return_value=True))
    @patch('os.listdir', Mock(return_value=['file', 'nginx-access-ui.log-20170630.log',
                                            'nginx-access-ui.log-20180215.bz2']))
    def test_unsupported_extensions(self):
        self.assertEqual(find_latest_logfile(self.path),
                         LogInfo('nginx-access-ui.log-20170630.log', datetime.datetime(2017,6,30), 'log'))


class TestConstructSettings(TestCase):

    def setUp(self):
        self.path = '/some/path'

    def test_no_custom_config(self):
        self.assertEqual(construct_settings(config, None), config)

    def test_no_config_file(self):
        with self.assertRaises((IOError, OSError)):
            construct_settings(config, self.path)

    def test_config_file_parse_error(self):
        mo = MagicMock(spec=StringIO, return_value=StringIO('<qwwe!'), read_data=StringIO('222222'))

        with patch('builtins.open', mo, create=True):
            with self.assertRaises(json.decoder.JSONDecodeError):
                construct_settings(config, self.path)

    def test_no_dict_in_file(self):
        expected_result = config.copy()

        mo = MagicMock(spec=StringIO, return_value=StringIO('11111'), read_data=StringIO('222222'))

        with patch('builtins.open', mo, create=True):
            self.assertEqual(construct_settings(config, self.path), expected_result)

    def test_reset_one_field(self):
        expected_result = config.copy()
        expected_result['REPORT_SIZE'] = 777

        mo = MagicMock(spec=StringIO, return_value=StringIO('{"REPORT_SIZE": 777}'), read_data=StringIO('222222'))

        with patch('builtins.open', mo, create=True):
            self.assertEqual(construct_settings(config, self.path), expected_result)

    def test_reset_all(self):
        expected_result = {
            "REPORT_SIZE": 500,
            "REPORT_DIR": "./new_reports",
            "LOG_DIR": "./new_logs",
            "ERROR_THRESHOLD": 10.0,
            "DEFAULT_CONFIG": "./config.json",
            "DEFAULT_REPORT_TEMPLATE": "./report.html"
        }

        file_content = '{"REPORT_SIZE": 500, "REPORT_DIR": "./new_reports", "LOG_DIR": "./new_logs", ' \
                       '"ERROR_THRESHOLD": 10.0}'

        mo = MagicMock(spec=StringIO, return_value=StringIO(file_content), read_data=StringIO('222222'))

        with patch('builtins.open', mo, create=True):
            self.assertEqual(construct_settings(config, self.path), expected_result)


class TestParseLine(TestCase):

    def test_valid_string(self):
        valid_sting = '1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/25019354 HTTP/1.1" 200 927' \
                      ' "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-"' \
                      ' "1498697422-2190034393-4708-9752759" "dc7161be3" 0.390'
        self.assertEqual(parse_line(valid_sting), ('/api/v2/banner/25019354', 0.39))

    def test_invalid_string(self):
        invalid_string = 'asdasd asas'
        self.assertEqual(parse_line(invalid_string), (None, None))

    def test_no_request_time_in_string(self):
        invalid_sting = '1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/25019354 HTTP/1.1" 200' \
                        ' 927 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-"' \
                        ' "1498697422-2190034393-4708-9752759" "dc7161be3"'
        self.assertEqual(parse_line(invalid_sting), (None, None))

    def test_no_url_in_expected_place(self):
        invalid_sting = '1.196.116.32 -  - "GET /api/v2/banner/25019354 HTTP/1.1" 200 927 "-" "Lynx/2.8.8dev.9' \
                        ' libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697422-2190034393-4708-9752759"' \
                        ' "dc7161be3" 0.390'
        self.assertEqual(parse_line(invalid_sting), (None, None))


class TestGetLogData(TestCase):

    def setUp(self):
        self.lines = [
            '1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/25019354 HTTP/1.1" 200 927 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697422-2190034393-4708-9752759" "dc7161be3" 0.390',
            '1.99.174.176 3b81f63526fa8  - [29/Jun/2017:03:50:22 +0300] "GET /api/1/photogenic_banners/list/?server_name=WIN7RB4 HTTP/1.1" 200 12 "-" "Python-urllib/2.7" "-" "1498697422-32900793-4708-9752770" "-" 0.133',
            '1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/16852664 HTTP/1.1" 200 19415 "-" "Slotovod" "-" "1498697422-2118016444-4708-9752769" "712e90144abee9" 0.199',
            '1.168.65.96 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/internal/banner/24294027/info HTTP/1.1" 200 407 "-" "-" "-" "1498697422-2539198130-4709-9928846" "89f7f1be37d" 0.146',
            '1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/16852664 HTTP/1.1" 200 1020 "-" "Configovod" "-" "1498697422-2118016444-4708-9752747" "712e90144abee9" 0.628'
        ]

        self.lines_with_error = [
            '1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/25019354 HTTP/1.1" 200 927 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697422-2190034393-4708-9752759" "dc7161be3" 0.390',
            '1.99.174.176 3b81f63526fa8  - [29/Jun/2017:03:50:22 +0300] "GET /api/1/photogenic_banners/list/?server_name=WIN7RB4 HTTP/1.1" 200 12 "-" "Python-urllib/2.7" "-" "1498697422-32900793-4708-9752770" "-" 0.133',
            '1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/16852664 HTTP/1.1" 200 19415 "-" "Slotovod" "-" "1498697422-2118016444-4708-9752769" "712e90144abee9" 0.199',
            '1.168.65.96 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/internal/banner/24294027/info HTTP/1.1" 200 407 "-" "-" "-" "1498697422-2539198130-4709-9928846" "89f7f1be37d" 0.146',
            '1.169.137.128 -  - "GET /api/v2/banner/16852664 HTTP/1.1" 200 1020 "-" "Configovod" "-" "1498697422-2118016444-4708-9752747" "712e90144abee9" 0.628'
        ]

    def test_valid_log(self):
        data = '\n'.join(self.lines)
        mo = MagicMock(spec=StringIO, return_value=StringIO(data), read_data=StringIO('222222'))

        with patch('builtins.open', mo, create=True):
            self.assertEqual([el for el in get_log_data('/some/path/log', 'log')],
                             [('/api/v2/banner/25019354', 0.390),
                              ('/api/1/photogenic_banners/list/?server_name=WIN7RB4', 0.133),
                              ('/api/v2/banner/16852664', 0.199),
                              ('/api/v2/internal/banner/24294027/info', 0.146),
                              ('/api/v2/banner/16852664', 0.628)])

    def test_log_with_error_string(self):
        data = '\n'.join(self.lines_with_error)
        mo = MagicMock(spec=StringIO, return_value=StringIO(data), read_data=StringIO('222222'))

        with patch('builtins.open', mo, create=True):
            self.assertEqual([el for el in get_log_data('/some/path/log', 'log')],
                             [('/api/v2/banner/25019354', 0.390),
                              ('/api/1/photogenic_banners/list/?server_name=WIN7RB4', 0.133),
                              ('/api/v2/banner/16852664', 0.199),
                              ('/api/v2/internal/banner/24294027/info', 0.146),
                              (None, None)])


class TestParseLogAndCollectTimings(TestCase):

    def setUp(self):
        self.lines = [
            '1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/25019354 HTTP/1.1" 200 927 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697422-2190034393-4708-9752759" "dc7161be3" 0.390',
            '1.99.174.176 3b81f63526fa8  - [29/Jun/2017:03:50:22 +0300] "GET /api/1/photogenic_banners/list/?server_name=WIN7RB4 HTTP/1.1" 200 12 "-" "Python-urllib/2.7" "-" "1498697422-32900793-4708-9752770" "-" 0.133',
            '1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/16852664 HTTP/1.1" 200 19415 "-" "Slotovod" "-" "1498697422-2118016444-4708-9752769" "712e90144abee9" 0.199',
            '1.168.65.96 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/internal/banner/24294027/info HTTP/1.1" 200 407 "-" "-" "-" "1498697422-2539198130-4709-9928846" "89f7f1be37d" 0.146',
            '1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/16852664 HTTP/1.1" 200 1020 "-" "Configovod" "-" "1498697422-2118016444-4708-9752747" "712e90144abee9" 0.628'
        ]

        self.lines_with_error = [
            '1.196.116.32 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/25019354 HTTP/1.1" 200 927 "-" "Lynx/2.8.8dev.9 libwww-FM/2.14 SSL-MM/1.4.1 GNUTLS/2.10.5" "-" "1498697422-2190034393-4708-9752759" "dc7161be3" 0.390',
            '1.99.174.176 3b81f63526fa8  - [29/Jun/2017:03:50:22 +0300] "GET /api/1/photogenic_banners/list/?server_name=WIN7RB4 HTTP/1.1" 200 12 "-" "Python-urllib/2.7" "-" "1498697422-32900793-4708-9752770" "-" 0.133',
            '1.169.137.128 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/banner/16852664 HTTP/1.1" 200 19415 "-" "Slotovod" "-" "1498697422-2118016444-4708-9752769" "712e90144abee9" 0.199',
            '1.168.65.96 -  - [29/Jun/2017:03:50:22 +0300] "GET /api/v2/internal/banner/24294027/info HTTP/1.1" 200 407 "-" "-" "-" "1498697422-2539198130-4709-9928846" "89f7f1be37d" 0.146',
            '1.169.137.128 -  - "GET /api/v2/banner/16852664 HTTP/1.1" 200 1020 "-" "Configovod" "-" "1498697422-2118016444-4708-9752747" "712e90144abee9" 0.628'
        ]

    def test_valid_log(self):
        data = '\n'.join(self.lines)
        mo = MagicMock(spec=StringIO, return_value=StringIO(data), read_data=StringIO('222222'))

        with patch('gzip.open', mo, create=True):
            self.assertEqual(parse_log_and_collect_timings('/some/path',
                                                           LogInfo('log', datetime.datetime(2017,6,30), 'gz')),
                             ({
                                 '/api/v2/banner/25019354': [0.390],
                                 '/api/1/photogenic_banners/list/?server_name=WIN7RB4': [0.133],
                                 '/api/v2/banner/16852664': [0.199, 0.628],
                                 '/api/v2/internal/banner/24294027/info': [0.146]
                             }, 0.0)
                             )

    def test_log_with_error_string(self):
        data = '\n'.join(self.lines_with_error)
        mo = MagicMock(spec=StringIO, return_value=StringIO(data), read_data=StringIO('222222'))

        with patch('gzip.open', mo, create=True):
            self.assertEqual(parse_log_and_collect_timings('/some/path',
                                                           LogInfo('log', datetime.datetime(2017,6,30), 'gz')),
                             ({
                                 '/api/v2/banner/25019354': [0.390],
                                 '/api/1/photogenic_banners/list/?server_name=WIN7RB4': [0.133],
                                 '/api/v2/banner/16852664': [0.199],
                                 '/api/v2/internal/banner/24294027/info': [0.146]
                             }, 20.0)
                             )

    def test_empty_log(self):
        data = ''
        mo = MagicMock(spec=StringIO, return_value=StringIO(data), read_data=StringIO('222222'))

        with patch('gzip.open', mo, create=True):
            self.assertEqual(parse_log_and_collect_timings('/some/path',
                                                           LogInfo('log', datetime.datetime(2017, 6, 30), 'gz')),
                             ({}, 0.0)
                             )


class TestAnalyzeTimings(TestCase):

    def setUp(self):
        self.sample_timings = {
                                 '/api/v2/banner/25019354': [0.390],
                                 '/api/1/photogenic_banners/list/?server_name=WIN7RB4': [0.133],
                                 '/api/v2/banner/16852664': [0.199],
                                 '/api/v2/internal/banner/24294027/info': [0.146, 0.186]
                             }

    def test_empty_timings(self):
        self.assertEqual(analyze_timings({}), [])

    def test_filled_timings(self):
        test_data = [
            {
             'url': '/api/v2/banner/25019354',
             'count': 1,
             'count_perc': 20.0,
             'time_sum': 0.390,
             'time_perc': 37.002,
             'time_avg': 0.390,
             'time_max': 0.390,
             'time_med': 0.390
            },
            {
             'url': '/api/1/photogenic_banners/list/?server_name=WIN7RB4',
             'count': 1,
             'count_perc': 20.0,
             'time_sum': 0.133,
             'time_perc': 12.619,
             'time_avg': 0.133,
             'time_max': 0.133,
             'time_med': 0.133
            },
            {
             'url': '/api/v2/banner/16852664',
             'count': 1,
             'count_perc': 20.0,
             'time_sum': 0.199,
             'time_perc': 18.880,
             'time_avg': 0.199,
             'time_max': 0.199,
             'time_med': 0.199
            },
            {
             'url': '/api/v2/internal/banner/24294027/info',
             'count': 2,
             'count_perc': 40.0,
             'time_sum': 0.332,
             'time_perc': 31.499,
             'time_avg': 0.166,
             'time_max': 0.186,
             'time_med': 0.166
            }
            ]

        self.assertEqual(analyze_timings(self.sample_timings), test_data)


class TestParseLog(TestCase):

    @patch('log_analyzer.find_latest_logfile', Mock(return_value=None))
    def test_no_log_dir(self):
        self.assertEqual(parse_log(config), None)

    @patch('log_analyzer.find_latest_logfile',
           Mock(return_value=LogInfo('filename', datetime.datetime(2017,6,30), 'gz')))
    @patch('log_analyzer.is_report_exists', Mock(return_value=True))
    def test_report_exists(self):
        self.assertEqual(parse_log(config), None)

    @patch('log_analyzer.find_latest_logfile',
           Mock(return_value=LogInfo('filename', datetime.datetime(2017, 6, 30), 'gz')))
    @patch('log_analyzer.is_report_exists', Mock(return_value=False))
    @patch('log_analyzer.parse_log_and_collect_timings', Mock(return_value=({}, 30.0)))
    def test_too_many_errors(self):
        with self.assertRaises(RuntimeError):
            parse_log(config)

    @patch('log_analyzer.find_latest_logfile',
           Mock(return_value=LogInfo('filename', datetime.datetime(2017, 6, 30), 'gz')))
    @patch('log_analyzer.is_report_exists', Mock(return_value=False))
    @patch('log_analyzer.parse_log_and_collect_timings', Mock(return_value=({}, 0.0)))
    @patch('log_analyzer.render_report_template', Mock(side_effect=FileNotFoundError))
    def test_failed_to_read_template(self):
        with self.assertRaises(RuntimeError):
            parse_log(config)

    @patch('log_analyzer.find_latest_logfile',
           Mock(return_value=LogInfo('filename', datetime.datetime(2017, 6, 30), 'gz')))
    @patch('log_analyzer.is_report_exists', Mock(return_value=False))
    @patch('log_analyzer.parse_log_and_collect_timings', Mock(return_value=({}, 0.0)))
    @patch('log_analyzer.render_report_template', Mock(return_value='some text'))
    @patch('log_analyzer.write_report', Mock(side_effect=IOError))
    def test_failed_to_write_report(self):
        with self.assertRaises(RuntimeError):
            parse_log(config)

    @patch('log_analyzer.find_latest_logfile',
           Mock(return_value=LogInfo('filename', datetime.datetime(2017, 6, 30), 'gz')))
    @patch('log_analyzer.is_report_exists', Mock(return_value=False))
    @patch('log_analyzer.parse_log_and_collect_timings', Mock(return_value=({}, 0.0)))
    @patch('log_analyzer.render_report_template', Mock(return_value='some text'))
    @patch('log_analyzer.write_report', Mock(return_value=None))
    def test_all_is_ok(self):
        self.assertEqual(parse_log(config), None)


class TestMain(TestCase):

    def setUp(self):
        if not os.path.exists(config['REPORT_DIR']):
            os.mkdir(config['REPORT_DIR'])

    def tearDown(self):
        if os.path.exists(config['REPORT_DIR']):
            shutil.rmtree(config['REPORT_DIR'])

    def test_help(self):
        with patch.object(sys, 'argv', ['./log_analyzer.py', '--help']):
            with self.assertRaises(SystemExit):
                main()

        with patch.object(sys, 'argv', ['./log_analyzer.py', '-h']):
            with self.assertRaises(SystemExit):
                main()

    def test_no_args(self):
        path = os.path.join(config['REPORT_DIR'], 'report-2017.06.30.html')
        if os.path.exists(path):
            os.remove(path)

        self.assertEqual(os.path.exists(path), False)
        self.assertEqual(main(), None)

        self.assertEqual(os.path.exists(path), True)

        with open(path, 'r') as f:
            data = f.read()
            self.assertTrue(len(data) > 0)

    def test_report_already_exists(self):
        path = os.path.join(config['REPORT_DIR'], 'report-2017.06.30.html')
        if not os.path.exists(path):
            f = open(path, 'w')
            f.close()

        self.assertEqual(os.path.exists(path), True)
        self.assertEqual(main(), None)
        self.assertEqual(os.path.exists(path), True)

    @patch.object(sys, 'argv', ['./log_analyzer.py', '--config', './config.yaml'])
    def test_custom_config_no_file(self):
        with self.assertRaises(SystemExit) as se:
            main()
            self.assertEqual(se.exception.code, 1)

    @patch.object(sys, 'argv', ['./log_analyzer.py', '--config', './config.cfg'])
    def test_custom_config_failed_to_parse(self):
        path = './config.cfg'
        if not os.path.exists(path):
            with open(path, 'wt') as f:
                f.write('some data')

        with self.assertRaises(SystemExit) as se:
            main()
            self.assertEqual(se.exception.code, 1)

        if os.path.exists(path):
            os.remove(path)

    def test_custom_config_no_logfile(self):
        with patch.object(sys, 'argv', ['./log_analyzer.py', '--config']):
            self.assertEqual(main(), None)

        with patch.object(sys, 'argv', ['./log_analyzer.py', '--config', './config.json']):
            self.assertEqual(main(), None)

        with patch.object(sys, 'argv', ['./log_analyzer.py', '--config=./config.json']):
            self.assertEqual(main(), None)
