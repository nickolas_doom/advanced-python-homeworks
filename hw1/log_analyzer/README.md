## Log Analyzer

### Описание

Простая утилита для анализа логов nginx и составления статистики по времени 
доступа к различным URL-адресам. Утилита анализирует наиболее свежий лог-файл 
в директории с логами с именем в формате `nginx-access-ui.log-<date>.<ext>`, где 
дата записана в формате `YYYYMMDD`, а расширение `gz` или `log`.

Утилита разбирает строки лога, соответствующие формату:
```
log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
                    '$request_time';
```

Любые другие строки считаются ошибочными и не попадают в результаты анализа. При слишком большом числе необработанных строк будет выведено соответствующее сообщение (порог настраивается через файл конфигурации).

### Запуск

- справка по интерфейсу командной строки:
  ```bash
  ./log_analyzer.py --help
  ```
- запуск без параметров - запуск анализа с дефолтными настройками, зашитыми в утилиту:
  ```bash
  ./log_analyzer.py
  ```
- запуск с кастомным конфигурационным файлом (если не указан путь до конфигурационного файла, то будет использован дефолтный - `./config.json`):
  ```bash
  ./log_analyzer.py --config [<path_to_config_file>]
  ```

### Формат конфигурационного файла

В качестве конфигурационных файлов поддерживаются тестовые файлы с содержимым в формате `json`. Поддерживаются следующие поля настроек:

```json
{
    "REPORT_SIZE": 1000,       // число URL попадающих в отчёт
    "REPORT_DIR": "./reports", // директория для записи отчёта
    "LOG_DIR": "./log",        // директория с логами для анализа
    "LOG_FILE": null,          // имя файла для вывода лога утилиты
    "ERROR_THRESHOLD": 20.0,   // порог для выхода по числу необработааных строк в процентах от общего числа
}
```
В файле настроек могут быть определены не все из указанных полей - в этом случае для недостающих настроек будут использованы дефолтные значения. Все ключи, не попадающие в указанный выше список, будут проигнорированы.