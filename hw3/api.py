#!/usr/bin/env python
# -*- coding: utf-8 -*-

import abc
import json
import datetime
import logging
import hashlib
import uuid
from optparse import OptionParser
from http.server import HTTPServer, BaseHTTPRequestHandler
from collections import namedtuple

import scoring

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}

MethodHandlerInfo = namedtuple('MethodHandlerInfo', ['constructor', 'handler'])


class ValidationError(Exception):
    """
    Ошибка валидации запроса/поля запроса.
    """
    def __init__(self, message):
        self.message = message


class Field:
    def __init__(self, name=None, required=False, nullable=False):
        self.name = name
        self.required = required
        self.nullable = nullable

    def __get__(self, inst, cls):
        return inst.__dict__.get(self.name)

    def __set__(self, inst, val):
        self.check_value(val)
        inst.__dict__[self.name] = val

    def check_value(self, val):
        if val == type(val)() and not self.nullable:
            raise ValidationError(f'nullable field {self.name} can\'t be null')

        if val is None and self.required:
            raise ValidationError(f'Required field {self.name} must not be None')


class CharField(Field):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)
        
        if val is not None and not isinstance(val, str):
            raise ValidationError(f'Field {self.name} must be of type string')


class ArgumentsField(Field):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)
        
        if not isinstance(val, dict):
            raise ValidationError(f'Field {self.name} must be of type dict')


class EmailField(CharField):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)
        
        if val is not None and len(val) > 0:
            if '@' not in val:
                raise ValidationError(f'Field {self.name} must contain @ symbol or be empty')


class PhoneField(Field):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)

        if val is not None and val != '':
            if isinstance(val, (str, int)):
                val = str(val)

                if len(val) != 11 or not val.startswith('7'):
                    raise ValidationError(f'Field {self.name} must be string of length 11 and starts with 7')

            else:
                raise ValidationError(f'Field {self.name} must be of type string or int')


class DateField(Field):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)

        if isinstance(val, str):
            try:
                datetime.datetime.strptime(val, '%d.%m.%Y')
            except ValueError:
                raise ValidationError(f'Field {self.name} must be date string of format DD.MM.YYYY')

        elif val is not None:
            raise ValidationError(f'Field {self.name} must be of type string or None')


class BirthDayField(DateField):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)

        if val is not None:
            date = datetime.datetime.strptime(val, '%d.%m.%Y')

            if (datetime.date.today().year - date.year) <= 0 or (datetime.date.today().year - date.year) > 70:
                raise ValidationError(f'Field {self.name} value must be valid'
                                      ' and no more than 70 years back from today')


class GenderField(Field):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)

        if isinstance(val, int):
            if val not in (0, 1, 2):
                raise ValidationError(f'Field {self.name} must be integer from list [0,1,2]')

        elif val is not None:
            raise ValidationError(f'Field {self.name} must be of type int or None')


class ClientIDsField(Field):
    def __init__(self, name=None, required=False, nullable=False):
        super().__init__(name, required, nullable)

    def check_value(self, val):
        super().check_value(val)

        if val is None:
            raise ValidationError(f'Field {self.name} must not be None')

        if not isinstance(val, list):
            raise ValidationError(f'Field {self.name} must be of type list')
        else:
            if len(val) == 0 or not any([isinstance(v, int) for v in val]):
                raise ValidationError(f'Field {self.name} must be non-empty list of integers')


class FieldsListConstructor(abc.ABCMeta):
    def __new__(mcs, name, bases, dct):
        dct['fields'] = [name for name, value in dct.items() if isinstance(dct[name], Field)]
        return super().__new__(mcs, name, bases, dct)


class Request(metaclass=FieldsListConstructor):
    def __init__(self, request):
        self.errors = []

        self.set_attributes(request)
        self.raise_errors()

    def raise_errors(self):
        if self.errors:
            raise ValidationError(f"{', '.join(self.errors)} fields are invalid")

    def set_attributes(self, request):
        for field in self.fields:
            try:
                setattr(self, field, request.get(field))
            except ValidationError:
                logging.exception('OnlineScoreRequest init error')
                self.errors.append(field)


class ClientsInterestsRequest(Request):
    client_ids = ClientIDsField(name='client_ids', required=True)
    date = DateField(name='date', required=False, nullable=True)


class OnlineScoreRequest(Request):
    first_name = CharField(name='first_name', required=False, nullable=True)
    last_name = CharField(name='last_name', required=False, nullable=True)
    email = EmailField(name='email', required=False, nullable=True)
    phone = PhoneField(name='phone', required=False, nullable=True)
    birthday = BirthDayField(name='birthday', required=False, nullable=True)
    gender = GenderField(name='gender', required=False, nullable=True)

    def check_structure(self):
        if not (bool(self.email and self.phone)
                or bool(self.first_name and self.last_name)
                or bool(self.gender in (0, 1, 2) and self.birthday)):
            raise ValidationError('Invalid OnlineScoreRequest structure')


class MethodRequest(Request):
    account = CharField(name='account', required=False, nullable=True)
    login = CharField(name='login', required=True, nullable=True)
    token = CharField(name='token', required=True, nullable=True)
    arguments = ArgumentsField(name='arguments', required=True, nullable=True)
    method = CharField(name='method', required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512((datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).encode()).hexdigest()
    else:
        digest = hashlib.sha512((request.account + request.login + SALT).encode()).hexdigest()

    logging.debug(f'digest : {digest}')
    logging.debug(f'token  : {request.token}')

    if digest == request.token:
        return True
    return False


def online_score_handler(method_request, request, ctx, store):
    try:
        method_request.check_structure()
    except ValidationError as exc:
        code = INVALID_REQUEST
        response = {'error': f'{str(exc)}'}
        return response, code

    if request.is_admin:
        score = 42
    else:
        score = scoring.get_score(
            store, method_request.phone, method_request.email, method_request.birthday,
            method_request.gender, method_request.first_name, method_request.last_name
        )

    response = {'score': score}
    code = OK

    ctx['has'] = [k for k, v in request.arguments.items() if v is not None]

    return response, code


def clients_interests_handler(method_request, request, ctx, store):
    response = {cid: scoring.get_interests(store, cid) for cid in method_request.client_ids}
    code = OK
    ctx['nclients'] = len(method_request.client_ids)

    return response, code


def method_handler(request, ctx, store):
    response, code = None, None
    request_body = request.get('body')

    handlers = {
        "online_score": MethodHandlerInfo(OnlineScoreRequest, online_score_handler),
        "clients_interests": MethodHandlerInfo(ClientsInterestsRequest, clients_interests_handler)
    }

    if not request_body:
        return {}, INVALID_REQUEST

    try:
        method_request = MethodRequest(request_body)

        if not check_auth(method_request):
            return {}, FORBIDDEN

        method = method_request.method
        if method in handlers:
            req = handlers[method].constructor(method_request.arguments)
            response, code = handlers[method].handler(req, method_request, ctx, store)

        else:
            code = NOT_FOUND

    except ValidationError as err:
        logging.exception('Failed to init request object')
        return {'error': str(err)}, INVALID_REQUEST

    logging.debug(f'<method_hander> code: {code}')
    logging.debug(f'<method_hander> response: {response}')

    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode())
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
