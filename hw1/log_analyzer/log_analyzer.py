#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip " '
#                     '[$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" '
#                     '"$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';

import os
import re
import sys
import gzip
import json
import shutil
import string
import logging
import tempfile
import argparse
import datetime
import statistics
import collections

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
    "ERROR_THRESHOLD": 20.0,
    "DEFAULT_CONFIG": "./config.json",
    "DEFAULT_REPORT_TEMPLATE": "./report.html"
}

LogInfo = collections.namedtuple('LogInfo', ['filename', 'date', 'extension'])


def configure_logging(settings):
    logging.basicConfig(format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S', level=logging.INFO,
                        filename=settings.get('LOG_FILE', None))


def find_latest_logfile(path):
    date_pattern = '(2[0-9][0-9][0-9])(0[1-9]|1[0-2])' \
                   '(0[1-9]|1[0-9]|2[0-9]|3[0-1])'
    pattern = f'nginx-access-ui.log-(?P<date>{date_pattern}).(?P<ext>log|gz)'
    report_match = re.compile(pattern)

    date = None
    filename = None
    ext = None

    if not os.path.exists(path):
        return None

    for f in os.listdir(path):
        m = report_match.match(f)

        if m is None:
            continue

        m_date = datetime.datetime.strptime(m.group('date'), "%Y%m%d")
        if date is None or m_date > date:
            date = m_date
            filename = f
            ext = m.group('ext')

    if not filename:
        return None

    return LogInfo(filename, date, ext)


def is_report_exists(date, directory):
    return os.path.exists(
        os.path.join(directory, f'report-{date.strftime("%Y.%m.%d")}.html')
    )


def construct_settings(settings: dict, path: str) -> dict:
    result = settings.copy()

    if not path:
        return result

    with open(path, 'rt') as f:
        custom_config = json.load(f)

        valid_fields = ['REPORT_SIZE', 'REPORT_DIR', 'LOG_DIR', 'LOG_FILE',
                        'ERROR_THRESHOLD']

        if isinstance(custom_config, dict):
            cc_sub = {k: v for k, v in custom_config.items() if k in valid_fields}

            result.update(cc_sub)

    return result


def parse_line(line: str):
    url_pattern = r'/(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|' \
                  r'(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    url_match = re.compile(url_pattern)

    parts = line.split()

    try:
        match = url_match.match(parts[6])
        if match:
            return parts[6], float(parts[-1])

    except IndexError:
        return None, None
    except ValueError:
        return None, None
    except Exception:
        logging.exception('Unexpected exception in log line parsing')
        return None, None

    return None, None


def get_log_data(filename, ext):
    log_open = gzip.open if ext == 'gz' else open

    with log_open(filename, 'rt') as f:
        for line in f:
            url, rtime = parse_line(line)

            yield url, rtime


def parse_log_and_collect_timings(path, log_info):
    logging.info('Parse log file and collect url timings info...')
    log_filename = os.path.join(path, log_info.filename)

    total_lines = 0
    error_lines = 0

    url_timings = collections.defaultdict(list)
    for url, rtime in get_log_data(log_filename, log_info.extension):
        total_lines += 1

        if url is None:
            error_lines += 1
            continue

        url_timings[url].append(rtime)

    logging.info(f'Total number of processed log lines: {total_lines}')
    logging.info(f'Total number of error log lines: {error_lines}')

    error_perc = 100.0 * error_lines / total_lines if total_lines > 0 else 0.0

    logging.info(f'Error percent: {error_perc}')

    return url_timings, error_perc


def analyze_timings(timings):
    logging.info('Analyze timings data...')
    url_stats = []

    total_count, total_time = 0, 0.0

    counts_and_time = ((len(timings[url]), sum(timings[url])) for url in timings)
    sums = [sum(values) for values in zip(*counts_and_time)]
    if sums:
        # return  url_stats

        total_count, total_time = sums

    for url in timings:
        count = len(timings[url])
        time = sum(timings[url])

        url_stats.append(
            {
                'url': url,
                'count': count,
                'count_perc': round(100 * float(count) / total_count, 3),
                'time_sum': round(time, 3),
                'time_perc': round(100 * time / total_time, 3),
                'time_avg': round(time / count, 3),
                'time_max': round(max(timings[url]), 3),
                'time_med': round(statistics.median(timings[url]), 3)
            }
        )

    return url_stats


def render_report_template(stats, settings):
    logging.info('Render report template...')

    template_filename = settings['DEFAULT_REPORT_TEMPLATE']
    report_size = settings['REPORT_SIZE']

    with open(template_filename, 'rb') as f:
        template = string.Template(f.read().decode('utf-8'))

        if template:
            filtered_stats = sorted(stats, key=lambda stat: stat['time_sum'],
                                    reverse=True)[:report_size]

            return template.safe_substitute({'table_json': str(filtered_stats)})


def write_report(report, date, report_dir):
    logging.info('Write report to disk...')
    report_filename = f'report-{date.strftime("%Y.%m.%d")}.html'

    filepath = os.path.join(report_dir, report_filename)

    with tempfile.NamedTemporaryFile(delete=False) as temp:
        temp.write(report.encode('utf-8'))
        temp.flush()

        shutil.copy(temp.name, filepath)


def parse_log(settings: dict):
    logging.info('Start analyzing logs...')
    log_info = find_latest_logfile(settings['LOG_DIR'])
    logging.debug(f'last log info: {log_info}')

    if log_info is None:
        logging.info('No log file found')
        return

    elif is_report_exists(log_info.date, settings['REPORT_DIR']):
        logging.info('Report for last log already exists')
        return

    logging.info('Collect url timings info from log file')
    url_timings_info, error_perc = parse_log_and_collect_timings(
        settings['LOG_DIR'], log_info
    )

    if error_perc > settings['ERROR_THRESHOLD']:
        raise RuntimeError(f'Too many unparsed log lines({error_perc}% errors'
                           f' with threshold in {settings["ERROR_THRESHOLD"]}%)')

    stats = analyze_timings(url_timings_info)
    try:
        report = render_report_template(stats, settings)
    except (IOError, OSError):
        raise RuntimeError('Failed to read report template')

    if report:
        try:
            write_report(report, log_info.date, settings['REPORT_DIR'])
        except (IOError, OSError):
            raise RuntimeError('Failed to write report to disk')


def main():
    parser = argparse.ArgumentParser(description='Simple nginx log analyzer')
    parser.add_argument('--config', type=str, nargs='?', dest='config',
                        const=config['DEFAULT_CONFIG'])

    arguments = parser.parse_args()

    config_path = arguments.config if arguments.config else None

    try:
        print('Configure log analyzer')
        settings = construct_settings(config, config_path)
        sys.stdout.flush()
        configure_logging(settings)
    except FileNotFoundError:
        sys.stderr.write("Can't read config file")
        sys.exit(1)
    except json.decoder.JSONDecodeError:
        sys.stderr.write("Can't parse config file")
        sys.exit(1)

    try:
        parse_log(settings)
    except RuntimeError as error:
        logging.error(error)
        sys.exit(1)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logging.exception('Manual keyboard interruption')
        sys.exit(1)
    except Exception:
        logging.exception('Unexpected error')
        sys.exit(1)
